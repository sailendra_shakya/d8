<?php
/**
 * @file
 * Contains \Drupal\related_block\Plugin\Block\RelatedBlock.
 */
namespace Drupal\related_block\Plugin\Block;
use Drupal\Core\Block\BlockBase;
/**
 * Provides a 'article' block.
 *
 * @Block(
 *   id = "testimonial_related_block",
 *   admin_label = @Translation("Related Custom Block"),
 *   category = @Translation("Related Custom Block")
 * )
 */
class RelatedBlock extends BlockBase {

  public function build() {
    $node_id = \Drupal::routeMatch()->getRawParameter('node');
    $node = \Drupal\node\Entity\Node::load($node_id);
    $node = \Drupal::routeMatch()->getParameter('node');
    $typeName = $node->bundle();
    if($typeName == 'testimonial'){
      $variable = $this->get_functionality($node, $node_id);
      $variable = (!empty($variable))?$variable:NULL;
      return array(
        '#theme' => 'related-block',
        '#variable' => $variable
        );
    }
  }





  private function get_functionality($node, $node_id){
    $testimonials = $node->get('field_related_stories')->referencedEntities();
    $testimonial = array();
    $i = 0;
    foreach ($testimonials as $testi) {
      $node_id = $testi->nid->value;
      $node = \Drupal\node\Entity\Node::load($node_id);
      $title_field = $node->get('title');
      $title = $title_field->value;

      $intro_field = $node->get('field_intro');
      $intro = $intro_field->value;

      $field_testimonial_image = $node->get('field_testimonial_image')->entity->uri->value;
      $image_path = file_create_url($field_testimonial_image);

      $intro_field = $node->get('field_intro');
      $intro = $intro_field->value;

      $options = ['absolute' => TRUE];
      $url = \Drupal\Core\Url::fromRoute('entity.node.canonical', ['node' => $node_id], $options);
      $url = $url->toString();



      $testimonial[$i]['image'] = $image_path;
      $testimonial[$i]['title'] = $title;
      $testimonial[$i]['intro'] = $intro;
      $testimonial[$i]['url'] = $url;
      $i++;
    }
    return $testimonial;

  }

}

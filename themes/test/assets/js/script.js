// Google map function
function initMap() {
 jQuery('.map-data-generate').each(function(index){
  var id = jQuery(this).attr('id');
  var address = jQuery(this).data('address');
  var zoom = jQuery(this).data('zoom');
  var draggable = jQuery(this).data('draggable');
  var disable_default_ui = jQuery(this).data('disable-default-ui');
  var scroll_wheel = jQuery(this).data('scroll-wheel');
  var disable_double_click_zoom = jQuery(this).data('disable-double-click-zoom');
  var icon = jQuery(this).data('icon');
  var styles = jQuery(this).data('styles');
  var baseUrl = jQuery(this).attr('data-baseUrl');
  var geocoder = new google.maps.Geocoder();

  var map = new google.maps.Map(document.getElementById(id), {
    zoom: zoom,
    disableDefaultUI: disable_default_ui,
    scrollwheel: scroll_wheel,
    draggable: 0,
    disableDoubleClickZoom: disable_double_click_zoom,
  });

  jQuery("#"+id).parent().before("<div id='direction_"+id+"' class='direction-cta'><a href='https://www.google.com/maps/place/"+address+"' target='_blank'>Get Directions</a></div>");

  geocoder.geocode({'address': address}, function(results, status) {
    var l = window.location;
    // var baseUrl = l.protocol + "//" + l.host + "/" + l.pathname.split('/')[1];
    var icon_url = baseUrl+'/themes/mjh/assets/images/pin.svg';
    console.log(icon_url);
    var image = {
      url: icon_url,
      size: new google.maps.Size(32, 46),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(0, 32)
    };
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      new google.maps.Marker({
        map: map,
        position: results[0].geometry.location,
        icon: image,
      });
    }
  });
});
}

  //check accordion open or not
  function check_accordion(){
    var length = jQuery('.field-collection-item--name-field-accordion-items .field--name-field-title').length();
    var count = jQuery('.field-collection-item--name-field-accordion-items .field--name-field-title .active').length();
    if(length == count){
      return true;
    }else{
      return false;
    }
  }


  // Sticky header
  function fixheader($fixheader, $slecter) {
   jQuery($fixheader).scroll(function() {
    var scroll = jQuery($fixheader).scrollTop();
    if (scroll >= 10 ) {
      jQuery($slecter).addClass("fix-header");

    } else {
      jQuery($slecter).removeClass("fix-header");

    }

  });
 }


// equalheight function
function equalheight(container){
  var currentTallest = 0,
  currentRowStart = 0,
  rowDivs = new Array(),
  $el,
  topPosition = 0;
  jQuery(container).each(function() {
    $el = jQuery(this);
    jQuery($el).height('auto');
    topPostion = $el.position().top;
    if (currentRowStart != topPostion) {
      for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
        rowDivs[currentDiv].height(currentTallest);
      }
        rowDivs.length = 0; // empty the array
        currentRowStart = topPostion;
        currentTallest = $el.height();
        rowDivs.push($el);
      } else {
        rowDivs.push($el);
        currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
      }
      for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
        rowDivs[currentDiv].height(currentTallest);
      }
    });
}

jQuery(document).ready(function(){

// Remove link from title of team detail page
// if(jQuery("#block-mjh-entityviewcontent").length)
// {
//   var title = jQuery("#block-mjh-entityviewcontent .node__title a").html();
//   jQuery("#block-mjh-entityviewcontent .node__title a").remove();
//   jQuery("#block-mjh-entityviewcontent .node__title").html(title);
// }

//Team detail hide contact block if there is no phone and email field
if(jQuery(".contact-details .field--name-field-phone").length == 0 && jQuery(".contact-details .field--name-field-email").length == 0){
  jQuery(".contact-details").hide();
}
// Form validation
jQuery.validator.addMethod("phoneno", function(phone_number, element) {
  phone_number = phone_number.replace(/\s+/g, "");
  return this.optional(element) || phone_number.length > 9 &&
  phone_number.match(/^((\ [1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
});

  //Arrange position of message block
  var message = jQuery('.messages__wrapper').html();
  if(message){
    jQuery('.messages__wrapper').remove();
    jQuery('#block-mjh_mychoice-content').prepend('<div class="messages__wrapper layout-container">'+message+'</div>');
  }

  //Mange Footer logo
  jQuery('.footer_logo .field--name-field-link a').html('');
  var link = jQuery('.footer_logo .field--name-field-link').html();
  jQuery('.footer_logo .field--name-field-logo img').wrap(link);
  jQuery('.footer_logo .field--name-field-link').remove();

  //Convert Image and Text field collection title to link and remove link
  jQuery(".field-collection-item--name-field-image-and-text-items .field--name-field-title").each(function(index,value){

    var presentSelector = jQuery(this);

    var title = presentSelector.html();
    var aTag = presentSelector.parent().find('a');
    var link = aTag.attr('href');
    aTag.remove();
    presentSelector.html('');

    if(link && link != 'undefined'){
     presentSelector.append('<a href="'+link+'">'+title+'</a>');
   }else{
     presentSelector.append(title);
   }
 });

  var owl = jQuery('.main-carousel');
  owl.owlCarousel({
   autoPlay: 6000,
   singleItem: true,
   navigation: true,
   pagination: true,
   loop: true,
 });



  // add fix header by taking page position
  var scrollPos = jQuery(document).scrollTop();
  if(scrollPos > 10){
    jQuery('.header').addClass('fix-header');
  }

  if (jQuery(window).width() > 991) {
    fixheader(window, '.header');
  }

  if (jQuery(window).width() > 991) {
    jQuery(window).scroll(function() {
      var scroll = jQuery(window).scrollTop();
      if (scroll >= 10 ) {
        jQuery('.breadcrumb-desktop').hide();

      } else {
        jQuery('.breadcrumb-desktop').show();

      }

    });
    jQuery('.breadcrumb-desktop').removeClass('hidden');
  } else {
    jQuery('.breadcrumb-desktop').addClass('hidden');
  }




   // Toogle funtion
   function toggleclass($toggleselector, $togglebody){
    jQuery(document).ready(function(){
      jQuery($toggleselector).click(function(){
        jQuery($togglebody).toggleClass('open');
      });
    });
  }

  // burgur icon open close
  toggleclass('#nav-btn','#nav-btn');
  toggleclass('#nav-btn','body');
  toggleclass('#nav-btn','.header');
  toggleclass('#nav-btn','.header-right');

  // to add downarrow on submenu in mobile menu
  jQuery('<i class="submenu-trigger"></i>').prependTo('.menu--primary-menu ul li.menu-item--expanded');

  // to open submenu
  jQuery('.menu--primary-menu i.submenu-trigger').click(function() {
    jQuery(this).parent().find('i.submenu-trigger').toggleClass('open');
    jQuery(this).parent().find('ul.menu:first').toggleClass('drop-menu-open');

  });

    // Follow link after second click
    if (jQuery(window).width() > 991 && jQuery(window).width() < 1025) {
      jQuery('.header-right .menu--primary-menu .menu li.menu-item--expanded > a').click(function(e){
        if(!jQuery(this).parent().hasClass('active')) {
          jQuery('.header-right .menu--primary-menu .menu li.menu-item--expanded').removeClass('active');
          jQuery(this).parent().addClass('active');
          e.preventDefault();
        } else {
          return true;
        }
      });
    }



  // table wrapper
  jQuery('table').wrap( '<div class="table-wrap"></div>' );

  // Accordian
  var headers = jQuery('.field-collection-item--name-field-accordion-items .field--name-field-title');
  var contentAreas = jQuery('.field-collection-item--name-field-accordion-items .field--name-field-body').hide();
  var expandLink = jQuery('.show-all');
  jQuery('.field--name-field-accordion-items > .field__item:first-child').find('.field--name-field-body').show();
  contentAreas.hide();

  headers.removeClass('active');
  jQuery(document).on('click','.field-collection-item--name-field-accordion-items .field--name-field-title',function() {
    jQuery(this).next().slideToggle('slow');

    if (jQuery(this).hasClass("active") === true) {
      jQuery(this).removeClass('active');
    } else {
      jQuery(this).addClass("active");
    }
    var length = jQuery('.field-collection-item--name-field-accordion-items .field--name-field-title').length;
    //field field--name-field-title field--type-string field--label-hidden field__item active
    var count = jQuery('.active').length;
    if(length === count){
     jQuery("#openallbutton").removeClass('show-all').addClass('close-all');
     jQuery("#openallbutton").html('Close All');
   }else if(count != 0){
     jQuery("#openallbutton").removeClass('close-all').addClass('show-all');
     jQuery("#openallbutton").html('Open All');
   }

   return false;
 }).next().hide();

  jQuery(document).on('click','.close-all',function(){
   contentAreas.hide();
   headers.removeClass('active');
   jQuery(this).removeClass('close-all').addClass('show-all');
   jQuery(this).html('Open All');
 });

  jQuery(document).on( "click", ".show-all", function() {
    contentAreas.show();
    headers.addClass('active');
    jQuery(this).removeClass('show-all').addClass('close-all');
    jQuery(this).html('Close All');
  });


   //To add Share text before Share this share icons
   jQuery( ".sharethis-wrapper" ).prepend( "<h3>Share</h3>" );

   jQuery.validator.addMethod("validEmail", function(value, element)
   {
    if(value == '')
      return true;
    var temp1;
    temp1 = true;
    var ind = value.indexOf('@');
    var str2=value.substr(ind+1);
    var str3=str2.substr(0,str2.indexOf('.'));
    if(str3.lastIndexOf('-')==(str3.length-1)||(str3.indexOf('-')!=str3.lastIndexOf('-')))
      return false;
    var str1=value.substr(0,ind);
    if((str1.lastIndexOf('_')==(str1.length-1))||(str1.lastIndexOf('.')==(str1.length-1))||(str1.lastIndexOf('-')==(str1.length-1)))
      return false;
    str = /(^[a-zA-Z0-9]+[\._-]{0,1})+([a-zA-Z0-9]+[_]{0,1})*@([a-zA-Z0-9]+[-]{0,1})+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,3})$/;
    temp1 = str.test(value);
    return temp1;
  }, "Please enter valid email.");

  //Book an appointment validate
  jQuery("#contact-message-book-an-appointment-form").validate({
    rules: {
      'field_name[0][value]': {
        required:true,
      },
      'field_email[0][value]': {
        required:true,
        validEmail: true
      },
      'field_phone[0][value]': {
        phoneno:true
      }

    },
    messages: {
      'field_name[0][value]': {
        required: "Please enter name",
      },
      'field_email[0][value]': {
        required: "Please enter email address",
        validEmail: "Please enter valid email address"
      },
      'field_phone[0][value]': {
        phoneno:"Please enter a valid Phone Number.",
      }
    },
    submitHandler: function(form) {
      form.submit();
    }
  });

  //Contact us validate
  jQuery("#contact-message-contact-form-form").validate({
    rules: {
      'field_name[0][value]': {
        required:true,
      },
      'field_email[0][value]': {
        required:true,
        validEmail: true,
      },
      'field_phone[0][value]': {
        phoneno:true
      },
      'field_office[0][value]': {
        required:true,
      }
    },
    messages: {
      'field_name[0][value]': {
        required: "Please enter name",
      },
      'field_email[0][value]': {
        required: "Please enter email address",
        validEmail: "Please enter valid email address"
      },
      'field_phone[0][value]': {
        phoneno:"Please enter a valid Phone Number.",
      },
      'field_office[0][value]': {
        required:"Please enter office name",
      }
    },
    submitHandler: function(form) {
      form.submit();
    }
  });




  // To maek equale ehight for primary featured fileds
  equalheight(".featured_block .field--name-field-featured-item-primary > .field__item");
  // To maek equale ehight for secondary featured fileds
  equalheight(".secondary_featured_block .field--name-field-featured-item > .field__item");
// To maek equale ehight for Team listing and testimonials listing
var blockHeight = jQuery('.testimonial-content-wrap').outerHeight();
if(jQuery(window).width() > 991) {
  jQuery('.block-mjh_testimonialblock  .field--name-field-testimonial-image, .block-testimonial-related-block .field--name-field-testimonial-image').css("min-height", blockHeight);
}




});




jQuery(window).resize(function(){

  if (jQuery(window).width() > 991) {
    fixheader(window, '.header');
  }

  // Follow link after second click
  if (jQuery(window).width() > 991 && jQuery(window).width() < 1025) {
    jQuery('.header-right .menu--primary-menu .menu li.menu-item--expanded > a').click(function(e){
      if(!jQuery(this).parent().hasClass('active')) {
        jQuery('.header-right .menu--primary-menu .menu li.menu-item--expanded').removeClass('active');
        jQuery(this).parent().addClass('active');
        e.preventDefault();
      } else {
        return true;
      }
    });
  }

// Hide breadcrumb on scroll
if (parseInt(jQuery(window).width()) < 991) {
  var scroll = jQuery(window).scrollTop();
  if (scroll >= 10 ) {
    jQuery('.breadcrumb-desktop').hide();
  }
}


equalheight(".featured_block .field--name-field-featured-item-primary > .field__item");
equalheight(".secondary_featured_block .field--name-field-featured-item > .field__item");



if (jQuery(window).width() > 991) {
  jQuery(window).scroll(function() {
    var scroll = jQuery(window).scrollTop();
    if (scroll >= 10 ) {
      jQuery('.breadcrumb-desktop').hide();

    } else {
      jQuery('.breadcrumb-desktop').show();

    }


  });
  jQuery('.breadcrumb-desktop').removeClass('hidden');
} else {
  jQuery('.breadcrumb-desktop').addClass('hidden');
}

var blockHeight = jQuery('.testimonial-content-wrap').outerHeight();
if(jQuery(window).width() > 991) {
  jQuery('.block-mjh_testimonialblock  .field--name-field-testimonial-image, .block-testimonial-related-block .field--name-field-testimonial-image').css("min-height", blockHeight);
}

});
